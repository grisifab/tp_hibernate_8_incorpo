package org.eclipse.main;



import org.eclipse.model.NomComplet;
import org.eclipse.model.Personne;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class App 
{
    public static void main( String[] args )
    {
    	/* Personne */ 
    	Personne personne = new Personne(); 
    	NomComplet nomComplet = new NomComplet(); 
    	nomComplet.setNom("travolta"); 
    	nomComplet.setPrenom("john"); 
    	personne.setNomComplet(nomComplet); 
    	
    	
    	Configuration configuration = new Configuration().configure(); 
    	SessionFactory sessionFactory = configuration.buildSessionFactory(); 
    	Session session = sessionFactory.openSession(); 
    	Transaction transaction = session.beginTransaction(); 
    	
    	session.persist(personne);
    	
    	transaction.commit(); 
    	session.close(); 
    	sessionFactory.close();

    }
}
