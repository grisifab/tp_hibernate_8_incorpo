package org.eclipse.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
 
public class Personne {
	
	private static int cnt;
	
	@Id
	@GeneratedValue (strategy=GenerationType.AUTO) 
	private int num;
	private NomComplet nomComplet;
	
	public static int getCnt() {
		return cnt;
	}
	
	public static void setCnt(int cnt) {
		Personne.cnt = cnt;
	}
	
	public int getNum() {
		return num;
	}
	
	public void setNum(int num) {
		this.num = num;
	}
	
	public NomComplet getNomComplet() {
		return nomComplet;
	}
	
	public void setNomComplet(NomComplet nomComplet) {
		this.nomComplet = nomComplet;
	}
	
	@Override
	public String toString() {
		return "Personne [num=" + num + ", nomComplet=" + nomComplet + "]";
	}
	
	public Personne(int num, NomComplet nomComplet) {
		super();
		this.num = num;
		this.nomComplet = nomComplet;
	}
	
	public Personne() {
		super();
		// TODO Auto-generated constructor stub
	}
}
